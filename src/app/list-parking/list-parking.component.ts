import { Component, OnInit } from '@angular/core';
import { ParkingLot } from '../models/ParkingLot';

@Component({
  selector: 'app-list-parking',
  templateUrl: './list-parking.component.html',
  styleUrls: ['./list-parking.component.css']
})
export class ListParkingComponent implements OnInit {

  allParkingLots: ParkingLot[] = []

  constructor() { }

  ngOnInit() {
    let storagelist = localStorage.getItem("parking-lots")
    let list: ParkingLot[] = []

    if (storagelist != null) {
      JSON.parse(storagelist).map((item: ParkingLot) => {
        list.push(new ParkingLot(
          item.veicleName, 
          item.veicleDriver, 
          item.veiclePlate, 
          item.contactPhone, 
          new Date(item.checkIn), 
          item.total, 
          item.checkOut
        ))
      })
    }

    this.allParkingLots = list
  }

  checkOut(p:ParkingLot) {
    p.doCheckOut();
    this.allParkingLots[this.allParkingLots.indexOf(p)] = p;
    console.log(this.allParkingLots)
    localStorage.setItem("parking-lots", JSON.stringify(this.allParkingLots))
  }

}
