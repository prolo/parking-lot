
export class ParkingLot {
    
    veicleName:string

    veicleDriver:string

    veiclePlate:string

    contactPhone:string

    checkIn:Date

    checkOut?:Date

    total?:number

    constructor(veicleName:string, veicleDriver:string, veiclePlate:string, contactPhone:string, checkIn:Date, total?:number, checkOut?:Date) {
        this.veicleName = veicleName
        this.veicleDriver = veicleDriver
        this.veiclePlate = veiclePlate
        this.contactPhone = contactPhone
        this.checkIn = checkIn
        if (total) this.total = total;
        if (checkOut) this.checkOut = checkOut;
    }

    doCheckOut() {
        if (this.checkOut == null) {
            this.checkOut = new Date()
            this.total = 4.50 + 4.50 * (this.checkOut.getHours() - this.checkIn.getHours())
        }
    }

}