import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { ListParkingComponent } from './list-parking/list-parking.component';
import { RegisterParkingComponent } from './register-parking/register-parking.component';

@NgModule({
  declarations: [
    AppComponent,
    ListParkingComponent,
    RegisterParkingComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
