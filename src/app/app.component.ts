import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ParkingLot } from './models/ParkingLot';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  
  parkingForm = this.formBuilder.group({
    veicleName: ['', [Validators.required]],
    veiclePlate: ['', [Validators.required, Validators.pattern("[A-Z]{3}[0-9][0-9A-Z][0-9]{2}")]],
    veicleDriver: ['', [Validators.required]],
    contactPhone: ['', [Validators.required]]
  })

  allParkingLots: ParkingLot[] = []

  constructor(private formBuilder: FormBuilder) {

  }

  ngOnInit() {
    let storagelist = localStorage.getItem("parking-lots")
    let list: ParkingLot[] = []

    if (storagelist != null) {
      JSON.parse(storagelist).map((item: ParkingLot) => {
        list.push(new ParkingLot(item.veicleName, item.veicleDriver, item.veiclePlate, item.contactPhone, new Date(item.checkIn)))
      })
    }

    this.allParkingLots = list
  }

  save() {
    if (this.parkingForm.valid) {
      let veicleName = this.parkingForm.value.veicleName
      let veiclePlate = this.parkingForm.value.veiclePlate
      let veicleDriver = this.parkingForm.value.veicleDriver
      let contactPhone = this.parkingForm.value.contactPhone

      let parkingLot = new ParkingLot(veicleName, veiclePlate, veicleDriver, contactPhone, new Date)

      this.allParkingLots.push(parkingLot);

      localStorage.setItem("parking-lots", JSON.stringify(this.allParkingLots))

      this.parkingForm.reset()
    }
  }

}
